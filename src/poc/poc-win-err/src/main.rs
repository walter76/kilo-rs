// https://docs.microsoft.com/en-us/windows/win32/debug/retrieving-the-last-error-code

use std::ffi::CStr;

extern crate winapi;

use winapi::shared::minwindef::DWORD;
use winapi::shared::ntdef::{HANDLE, NULL, PSTR};
use winapi::um::errhandlingapi::GetLastError;
use winapi::um::processthreadsapi::GetProcessId;
use winapi::um::winbase::{
    FormatMessageA, FORMAT_MESSAGE_ALLOCATE_BUFFER, FORMAT_MESSAGE_FROM_SYSTEM,
    FORMAT_MESSAGE_IGNORE_INSERTS,
};

// https://github.com/Rust-WinGUI/win32-error/blob/master/src/lib.rs
// https://stackoverflow.com/questions/24145823/how-do-i-convert-a-c-string-into-a-rust-string-and-back-via-ffi

// This example works and I get a valid string.
//
// TODO need to take care about freeing the memory that has been allocated by the Windows Kernel
//
fn get_last_error() -> DWORD {
    let err_code: DWORD = unsafe { GetLastError() };
    let mut lp_buffer: PSTR = std::ptr::null_mut();

    let buffer_size = unsafe {
        FormatMessageA(
            FORMAT_MESSAGE_ALLOCATE_BUFFER
                | FORMAT_MESSAGE_FROM_SYSTEM
                | FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            err_code,
            0,
            // lp_buffer,  // Will not work, because this is passing the value, which is the
                           // null pointer in this case. So, the WinAPI will simply fail.
            <*mut _>::cast(&mut lp_buffer),  // https://doc.rust-lang.org/std/primitive.pointer.html#method.cast
            0,
            std::ptr::null_mut(),
        )
    };

    println!("{}", buffer_size);

    if lp_buffer == std::ptr::null_mut() {
        println!("is null");
    }

    let msg = match lp_buffer {
        lp_buffer if lp_buffer != std::ptr::null_mut() => unsafe {
            let c_str: &CStr = CStr::from_ptr(lp_buffer);
            String::from(c_str.to_str().unwrap())
        },
        _ => String::from(""),
    };

    println!("{}", msg);

    err_code
}

// This example works in the sense that the WinAPI returns a valid result, but I
// have not found a working way to convert the Vec<i8> to a valid string that
// can be handled in rust. It always panics.
fn get_last_error_with_vec() -> DWORD {
    let err_code: DWORD = unsafe { GetLastError() };
    let mut lp_buffer: Vec<i8> = Vec::with_capacity(1_024);

    let buffer_size = unsafe {
        FormatMessageA(
            FORMAT_MESSAGE_ALLOCATE_BUFFER
                | FORMAT_MESSAGE_FROM_SYSTEM
                | FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            err_code,
            0,
            lp_buffer.as_mut_ptr(),
            0,
            std::ptr::null_mut(),
        )
    };

    unsafe { lp_buffer.set_len(buffer_size as usize) };

    println!("{}", buffer_size);
    println!("{:?}", lp_buffer);

    // This fails and panics:
    // thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: FromUtf8Error
    // { bytes: [32, 217, 160, 136, 65, 1, 0, 0, 64, 222, 160, 136, 65, 1, 0, 0, 67, 0, 58,
    // 0, 92, 0, 85, 0], error: Utf8Error { valid_up_to: 3, error_len: Some(1) } }',
    // src\main.rs:87:37
    // let lp_buffer_u8 = lp_buffer.into_iter().map(|i| i as u8).collect::<Vec<u8>>();
    // String::from_utf8(lp_buffer_u8).unwrap();

    // This fails and panics:
    // thread 'main' panicked at 'called `Result::unwrap()` on an `Err` value: Utf8Error
    //  { valid_up_to: 1, error_len: Some(1) }', src\main.rs:94:43
    let c_str: &CStr = unsafe { CStr::from_ptr(lp_buffer.as_slice().as_ptr()) };
    let msg = String::from(c_str.to_str().unwrap());
    println!("{}", msg);

    err_code
}

// This example works in the sense that the WinAPI returns a valid buffer size, but the
// content is not the same as with the `get_last_error_with_vec()` example. Same problem
// as above. I have not found a working way to convert the Vec<i8> to a valid string
// taht can be handled in rust. It always panics.
fn get_last_error_with_arr() -> DWORD {
    let err_code: DWORD = unsafe { GetLastError() };
    let mut lp_buffer: [i8; 1_024] = [0; 1_024];

    let buffer_size = unsafe {
        FormatMessageA(
            FORMAT_MESSAGE_ALLOCATE_BUFFER
                | FORMAT_MESSAGE_FROM_SYSTEM
                | FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            err_code,
            0,
            lp_buffer.as_mut_ptr(),
            0,
            std::ptr::null_mut(),
        )
    };

    println!("{}", buffer_size);
    println!("{:?}", lp_buffer);

    // This does not panic, but it does not print the expected string `The handle is
    // invalid.`. Actually it prints an empty string.
    let c_str: &CStr = unsafe { CStr::from_ptr(lp_buffer.as_ptr()) };
    let msg = String::from(c_str.to_str().unwrap());
    println!("{}", msg);

    err_code
}

fn get_process_id(handle: HANDLE) -> DWORD {
    unsafe { GetProcessId(handle) }
}

fn main() {
    get_process_id(NULL);
    // let err_code = get_last_error_with_vec();
    // let err_code = get_last_error_with_arr();
    let err_code = get_last_error();
    println!("{:#x}", err_code);
}
