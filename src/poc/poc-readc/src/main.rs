extern crate winapi;

use winapi::shared::minwindef::{DWORD, TRUE, FALSE};
use winapi::um::wincontypes::{KEY_EVENT_RECORD};

// https://docs.microsoft.com/de-de/windows/console/reading-input-buffer-events

fn key_event_proc(key_event: &KEY_EVENT_RECORD) -> bool {
    let mut result = true;
    let c_val = unsafe { *key_event.uChar.UnicodeChar() };
    let c = std::char::from_u32(c_val as u32).unwrap();

    println!("Key event: ");
    println!("{} ({})", c, c_val);

    if key_event.bKeyDown == TRUE {
        println!("key pressed");
    } else {
        println!("key released");

        if c == 'q' {
            result = false;
        }
    }

    result
}

fn main() {
    use winapi::um::consoleapi::{GetConsoleMode, SetConsoleMode, ReadConsoleInputW};
    use winapi::um::processenv::GetStdHandle;
    use winapi::um::winbase::STD_INPUT_HANDLE;
    use winapi::um::wincontypes::{INPUT_RECORD, KEY_EVENT, MOUSE_EVENT, WINDOW_BUFFER_SIZE_EVENT, FOCUS_EVENT, MENU_EVENT};
    use winapi::um::handleapi::INVALID_HANDLE_VALUE;
    use winapi::um::wincon::{ENABLE_WINDOW_INPUT, ENABLE_MOUSE_INPUT};

    let h_stdin = unsafe { GetStdHandle(STD_INPUT_HANDLE) };
    if h_stdin == INVALID_HANDLE_VALUE {
        panic!("Unable to get handle for stdin!");
    }

    let mut save_old_console_mode: DWORD = 0;
    if unsafe { GetConsoleMode(h_stdin, &mut save_old_console_mode) } == FALSE {
        panic!("Unable to get console mode!");
    };

    let console_mode: DWORD = ENABLE_WINDOW_INPUT | ENABLE_MOUSE_INPUT;
    if unsafe { SetConsoleMode(h_stdin, console_mode) } == FALSE {
        panic!("Unable to set console mode!");
    }

    unsafe {
        let mut input_record: Vec<INPUT_RECORD> = Vec::with_capacity(128);
        let mut number_of_events_read: DWORD = 0;
        let mut count = 0;

        loop {
            let result = ReadConsoleInputW(
                h_stdin,
                input_record.as_mut_ptr(),
                128,
                &mut number_of_events_read,
            );
            if result == 0 {
                panic!("ReadConsoleInput");
            }

            input_record.set_len(number_of_events_read as usize);
            println!("{} input events read", number_of_events_read);

            for ir in input_record.iter() {
                match ir.EventType {
                    KEY_EVENT => {
                        let key_event = ir.Event.KeyEvent();
                        if !key_event_proc(key_event) {
                            count = 101;
                            break;
                        }
                    },
                    MOUSE_EVENT => {
                        println!("mouse event")
                    },
                    WINDOW_BUFFER_SIZE_EVENT => {
                        println!("window buffer size event")
                    },
                    FOCUS_EVENT => {
                        println!("focus event")
                    },
                    MENU_EVENT => {
                        println!("menu event")
                    }
                    _ => println!("unknown event read"),
                }
            }

            if count > 100 {
                break;
            }

            count += 1;
        }
    }

    if unsafe { SetConsoleMode(h_stdin, save_old_console_mode) } == FALSE {
        panic!("Unable to restore original console mode!");
    }
}
