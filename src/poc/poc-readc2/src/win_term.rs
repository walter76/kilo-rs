extern crate winapi;

use winapi::shared::minwindef::{DWORD, FALSE};
use winapi::shared::ntdef::HANDLE;
use winapi::um::consoleapi::{GetConsoleMode, ReadConsoleA, SetConsoleMode};
use winapi::um::handleapi::INVALID_HANDLE_VALUE;
use winapi::um::processenv::GetStdHandle;
use winapi::um::winbase::{STD_INPUT_HANDLE, STD_OUTPUT_HANDLE};
use winapi::um::wincon::{
    CONSOLE_READCONSOLE_CONTROL, ENABLE_ECHO_INPUT, ENABLE_EXTENDED_FLAGS, ENABLE_LINE_INPUT,
    ENABLE_PROCESSED_INPUT, ENABLE_PROCESSED_OUTPUT, ENABLE_VIRTUAL_TERMINAL_INPUT,
    ENABLE_VIRTUAL_TERMINAL_PROCESSING, ENABLE_WRAP_AT_EOL_OUTPUT,
};

use std::io;

// todo: learn about error handling and add proper one here

pub struct WinTerm {
    h_stdin: HANDLE,
    original_stdin_console_mode: DWORD,
    h_stdout: HANDLE,
    original_stdout_console_mode: DWORD,
}

impl WinTerm {
    // TODO add some console output if console mode cannot be saved
    pub fn new() -> Self {
        let h_stdin = WinTerm::get_stdin_handle().expect("Cannot get handle for stdin.");
        let original_stdin_console_mode = match WinTerm::get_console_mode(h_stdin) {
            Some(console_mode) => console_mode,
            None => 0x0,
        };

        let h_stdout = WinTerm::get_stdout_handle().expect("Cannot get handle for stdout");
        let original_stdout_console_mode = match WinTerm::get_console_mode(h_stdout) {
            Some(console_mode) => console_mode,
            None => 0x0,
        };

        Self {
            h_stdin,
            original_stdin_console_mode,
            h_stdout,
            original_stdout_console_mode,
        }
    }

    fn get_stdin_handle() -> Result<HANDLE, io::Error> {
        WinTerm::get_std_handle(STD_INPUT_HANDLE)
    }

    fn get_stdout_handle() -> Result<HANDLE, io::Error> {
        WinTerm::get_std_handle(STD_OUTPUT_HANDLE)
    }

    fn get_std_handle(handle_type: DWORD) -> Result<HANDLE, io::Error> {
        let h_std = unsafe { GetStdHandle(handle_type) };

        if h_std == INVALID_HANDLE_VALUE {
            return Err(std::io::Error::new(
                io::ErrorKind::Other,
                "cannot get handle from operating system",
            ));
        }

        Ok(h_std)
    }

    fn get_console_mode(handle: HANDLE) -> Option<DWORD> {
        let mut console_mode: DWORD = 0x0;

        if unsafe { GetConsoleMode(handle, &mut console_mode) } == FALSE {
            return None;
        }

        Some(console_mode)
    }

    // TODO add some console output if console mode cannot be saved
    pub fn enable_virtual_terminal(&self) {
        let stdin_current_console_mode = match WinTerm::get_console_mode(self.h_stdin) {
            Some(console_mode) => console_mode,
            None => 0x0,
        };
        let stdin_console_mode = stdin_current_console_mode | ENABLE_VIRTUAL_TERMINAL_INPUT;
        WinTerm::set_console_mode(self.h_stdin, stdin_console_mode);

        let stdout_current_console_mode = match WinTerm::get_console_mode(self.h_stdout) {
            Some(console_mode) => console_mode,
            None => 0x0,
        };
        let stdout_console_mode = stdout_current_console_mode | ENABLE_VIRTUAL_TERMINAL_PROCESSING;
        WinTerm::set_console_mode(self.h_stdout, stdout_console_mode);
    }

    // TODO add some console output if console mode cannot be saved
    pub fn enable_raw_mode(&self) {
        let stdin_current_console_mode = match WinTerm::get_console_mode(self.h_stdin) {
            Some(console_mode) => console_mode,
            None => 0x0,
        };
        let stdin_raw_mode = stdin_current_console_mode
            & !(ENABLE_ECHO_INPUT
                | ENABLE_LINE_INPUT
                | ENABLE_PROCESSED_INPUT
                | ENABLE_EXTENDED_FLAGS);
        WinTerm::set_console_mode(self.h_stdin, stdin_raw_mode);

        let stdout_current_console_mode = match WinTerm::get_console_mode(self.h_stdout) {
            Some(console_mode) => console_mode,
            None => 0x0,
        };
        let stdout_raw_mode =
            stdout_current_console_mode & !(ENABLE_PROCESSED_OUTPUT | ENABLE_WRAP_AT_EOL_OUTPUT);
        WinTerm::set_console_mode(self.h_stdout, stdout_raw_mode);
    }

    pub fn read_c(&self) -> Option<char> {
        let read_buffer = self.read_console(1);
        Some(read_buffer?.into_iter().nth(0)? as char)
    }

    pub fn read_console(&self, number_of_chars_to_read: usize) -> Option<Vec<u8>> {
        let mut number_of_chars_read: DWORD = 0x0;
        let mut read_buffer: Vec<u8> = Vec::with_capacity(number_of_chars_to_read);
        let mut input_control = CONSOLE_READCONSOLE_CONTROL {
            nLength: 0,
            nInitialChars: 0,
            dwCtrlWakeupMask: 0,
            dwControlKeyState: 0,
        };

        unsafe {
            if ReadConsoleA(
                self.h_stdin,
                read_buffer.as_mut_ptr() as *mut winapi::ctypes::c_void,
                number_of_chars_to_read as u32,
                &mut number_of_chars_read,
                &mut input_control,
            ) == FALSE
            {
                return None;
            }

            read_buffer.set_len(number_of_chars_read as usize);
        }

        Some(read_buffer)
    }

    pub fn restore_orig_console_modes(&self) {
        WinTerm::set_console_mode(self.h_stdin, self.original_stdin_console_mode);
        WinTerm::set_console_mode(self.h_stdout, self.original_stdout_console_mode);
    }

    fn set_console_mode(handle: HANDLE, console_mode: DWORD) {
        if unsafe { SetConsoleMode(handle, console_mode) } == FALSE {
            panic!(format!(
                "cannot set console mode 0x{:02X?} for handle {:?}",
                console_mode, handle
            ));
        }
    }
}

impl Drop for WinTerm {
    fn drop(&mut self) {
        self.restore_orig_console_modes();
    }
}