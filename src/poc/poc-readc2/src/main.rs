mod win_term;

use win_term::WinTerm;

fn main() {
    let term = WinTerm::new();
    term.enable_virtual_terminal();
    term.enable_raw_mode();

    loop {
        let c = match term.read_c() {
            Some(c) => c,
            None => continue
        };
        
        if c.is_control() {
            println!("0x{:02X?}\r", c as u8);
        } else {
            println!("0x{:02X?} ('{}')\r", c as u8, c);
        }

        if c == 'q' {
            break;
        }
    }

    term.restore_orig_console_modes();
}
