use std::env;
use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use crate::terminal::{KeyEvent, Terminal, Writer};

use crate::{EDITOR_DEFAULT_HEIGHT, EDITOR_DEFAULT_WIDTH, EDITOR_VERSION};

pub struct Config {
    file_path: Option<String>,
}

impl Config {
    pub fn new(mut args: env::Args) -> Self {
        args.next();

        if let Some(file_path) = args.next() {
            Self {
                file_path: Some(file_path),
            }
        } else {
            Self { file_path: None }
        }
    }
}

pub fn run(term: &impl Terminal, config: &Config) {
    let mut editor = Editor::new(term);

    if let Some(file_path) = &config.file_path {
        editor.open_file(file_path);
    }

    loop {
        editor.update_state();
        editor.refresh_screen(term);

        match term.read_event() {
            Some(key_event) => {
                if !editor.process_key_event(key_event) {
                    break;
                }
            }
            None => continue,
        }
    }

    term.clear_screen();
}

struct State {
    screen_rows: usize,
    screen_cols: usize,
    cx: usize,
    cy: usize,
    rows: Vec<String>,
    row_offset: usize,
    col_offset: usize,
}

impl State {
    pub fn new(screen_rows: usize, screen_cols: usize) -> Self {
        Self {
            screen_rows,
            screen_cols,
            cx: 0,
            cy: 0,
            rows: Vec::new(),
            row_offset: 0,
            col_offset: 0,
        }
    }
}

struct Editor {
    state: State,
    writer: Writer,
}

impl Editor {
    pub fn new(term: &impl Terminal) -> Self {
        match term.window_size() {
            Some(ws) => Self {
                state: State::new(ws.height, ws.width),
                writer: Writer::with_size(ws.height, ws.width),
            },
            None => Self {
                state: State::new(EDITOR_DEFAULT_HEIGHT, EDITOR_DEFAULT_WIDTH),
                writer: Writer::with_size(EDITOR_DEFAULT_HEIGHT, EDITOR_DEFAULT_WIDTH),
            },
        }
    }

    pub fn update_state(&mut self) {
        self.scroll();
    }

    fn scroll(&mut self) {
        if self.state.cy < self.state.row_offset {
            self.state.row_offset = self.state.cy;
        }
        if self.state.cy >= self.state.row_offset + self.state.screen_rows {
            self.state.row_offset = self.state.cy - self.state.screen_rows + 1;
        }
        if self.state.cx < self.state.col_offset {
            self.state.col_offset = self.state.cx;
        }
        if self.state.cx >= self.state.col_offset + self.state.screen_cols {
            self.state.col_offset = self.state.cx - self.state.screen_cols + 1;
        }
    }

    pub fn refresh_screen(&mut self, term: &impl Terminal) {
        self.writer.hide_cursor();

        self.writer.reset_cursor();

        if self.state.rows.is_empty() {
            self.draw_welcome_screen()
        } else {
            self.draw_rows();
        }

        self.writer.set_cursor_pos(
            (self.state.cx - self.state.col_offset) + 1,
            (self.state.cy - self.state.row_offset) + 1,
        );

        self.writer.show_cursor();

        self.writer.present(term);
    }

    fn draw_welcome_screen(&mut self) {
        for y in 0..self.state.screen_rows {
            if self.state.rows.is_empty() && y == self.state.screen_rows / 3 {
                self.print_welcome_message();
            } else {
                self.writer.write("~");
            }

            self.writer.erase_till_eol();
            if y < self.state.screen_rows - 1 {
                self.writer.write("\r\n");
            }
        }
    }

    fn print_welcome_message(&mut self) {
        let welcome_message = format!("Kilo editor -- version {}", EDITOR_VERSION);

        if welcome_message.len() > self.state.screen_cols {
            self.writer
                .write(&welcome_message[..self.state.screen_cols]);
        } else {
            self.print_centered(welcome_message.as_str());
        }
    }

    fn print_centered(&mut self, str: &str) {
        let mut padding = (self.state.screen_cols - str.len()) / 2;
        if padding > 0 {
            self.writer.write("~");
            padding -= 1;
        }

        self.writer
            .write(format!("{:>width$}", str, width = padding + str.len()).as_str());
    }

    fn draw_rows(&mut self) {
        for y in 0..self.state.screen_rows {
            let buffer_y = y + self.state.row_offset;

            if buffer_y >= self.state.rows.len() {
                self.writer.write("~");
            } else {
                let row = &self.state.rows[buffer_y];
                let buffer_x_start = self.state.col_offset;

                if buffer_x_start < row.len() {
                    let buffer_x_end = buffer_x_start + self.state.screen_cols;

                    if buffer_x_end >= row.len() {
                        self.writer.write(&row[buffer_x_start..]);
                    } else {
                        self.writer.write(&row[buffer_x_start..buffer_x_end]);
                    }
                }
            }

            self.writer.erase_till_eol();
            if y < self.state.screen_rows - 1 {
                self.writer.write("\r\n");
            }
        }
    }

    pub fn process_key_event(&mut self, key_event: KeyEvent) -> bool {
        match key_event {
            KeyEvent::Char(c) => {
                if Editor::is_ctrl_key(c, 'q') {
                    return false;
                }
            }
            e @ KeyEvent::ArrowUp
            | e @ KeyEvent::ArrowDown
            | e @ KeyEvent::ArrowRight
            | e @ KeyEvent::ArrowLeft => self.move_cursor(e),
            KeyEvent::PageDown => self.state.cy = self.state.screen_rows - 1,
            KeyEvent::PageUp => self.state.cy = 0,
            KeyEvent::Home => self.state.cx = 0,
            KeyEvent::End => self.state.cx = self.state.screen_cols - 1,
            KeyEvent::Del => (),
        }

        true
    }

    fn is_ctrl_key(in_key_c: char, c_to_map: char) -> bool {
        let in_key_c = in_key_c as u8;
        let c_to_map = c_to_map as u8;

        in_key_c == c_to_map & 0x1f
    }

    fn move_cursor(&mut self, key_event: KeyEvent) {
        match key_event {
            KeyEvent::ArrowLeft => {
                if self.state.cx > 0 {
                    self.state.cx -= 1;
                }
            }
            KeyEvent::ArrowRight => {
                if self.state.cy < self.state.rows.len()
                    && self.state.cx < self.state.rows[self.state.cy].len()
                {
                    self.state.cx += 1;
                }
            }
            KeyEvent::ArrowUp => {
                if self.state.cy > 0 {
                    self.state.cy -= 1;
                }
            }
            KeyEvent::ArrowDown => {
                if self.state.cy < self.state.rows.len() {
                    self.state.cy += 1;
                }
            }
            _ => (),
        }
    }

    fn open_file<P>(&mut self, file_name: P)
    where
        P: AsRef<Path>,
    {
        self.state.rows.clear();

        if let Ok(lines) = Editor::read_lines(file_name) {
            for line in lines.flatten() {
                self.state.rows.push(line);
            }
        }
    }

    fn read_lines<P>(file_name: P) -> io::Result<io::Lines<io::BufReader<File>>>
    where
        P: AsRef<Path>,
    {
        let file = File::open(file_name)?;
        Ok(io::BufReader::new(file).lines())
    }
}
