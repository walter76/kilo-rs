pub struct WindowSize {
    pub height: usize,
    pub width: usize,
}

pub enum KeyEvent {
    Char(char),
    ArrowUp,
    ArrowDown,
    ArrowLeft,
    ArrowRight,
    PageUp,
    PageDown,
    Home,
    End,
    Del,
}

pub trait Terminal {
    fn window_size(&self) -> Option<WindowSize>;
    fn clear_screen(&self);
    fn read_c(&self) -> Option<char>;
    fn write(&self, str: &str) -> Option<u32>;

    fn read_event(&self) -> Option<KeyEvent> {
        let c = match self.read_c() {
            Some(c) => c,
            None => return None,
        };

        let mut key_event = KeyEvent::Char(c);

        if c == '\x1b' {
            let mut ctrl_seq: Vec<char> = Vec::new();
            for _ in 0..2 {
                ctrl_seq.push(
                    self.read_c()
                        .expect("unable to read rest of control sequence"),
                );
            }

            if ctrl_seq[0] == '[' {
                if ctrl_seq[1] >= '0' && ctrl_seq[1] <= '9' {
                    ctrl_seq.push(
                        self.read_c()
                            .expect("unable to read rest of control sequence"),
                    );
                    if ctrl_seq[2] == '~' {
                        match ctrl_seq[1] {
                            '1' => key_event = KeyEvent::Home,
                            '3' => key_event = KeyEvent::Del,
                            '4' => key_event = KeyEvent::End,
                            '5' => key_event = KeyEvent::PageUp,
                            '6' => key_event = KeyEvent::PageDown,
                            '7' => key_event = KeyEvent::Home,
                            '8' => key_event = KeyEvent::End,
                            _ => (),
                        }
                    }
                } else {
                    match ctrl_seq[1] {
                        'A' => key_event = KeyEvent::ArrowUp,
                        'B' => key_event = KeyEvent::ArrowDown,
                        'C' => key_event = KeyEvent::ArrowRight,
                        'D' => key_event = KeyEvent::ArrowLeft,
                        'H' => key_event = KeyEvent::Home,
                        'F' => key_event = KeyEvent::End,
                        _ => (),
                    }
                }
            } else if ctrl_seq[0] == 'O' {
                match ctrl_seq[1] {
                    'H' => key_event = KeyEvent::Home,
                    'F' => key_event = KeyEvent::End,
                    _ => (),
                }
            }
        }

        Some(key_event)
    }
}

#[derive(Debug)]
pub enum VirtualTerminalSequence {
    ClearScreen,
    CursorForward(usize),
    CursorDown(usize),
    ResetCursor,
    ReportCursorPosition,
    TextCursorEnableModeShow,
    TextCursorEnableModeHide,
    CursorPosition { cy: usize, cx: usize },
    EraseInLine(Option<usize>),
}

impl VirtualTerminalSequence {
    pub fn to_command(&self) -> String {
        match self {
            VirtualTerminalSequence::ClearScreen => String::from("\x1b[2J"),
            VirtualTerminalSequence::CursorForward(steps) => format!("\x1b[{}C", steps),
            VirtualTerminalSequence::CursorDown(steps) => format!("\x1b[{}B", steps),
            VirtualTerminalSequence::ResetCursor => String::from("\x1b[H"),
            VirtualTerminalSequence::ReportCursorPosition => String::from("\x1b[6n"),
            VirtualTerminalSequence::TextCursorEnableModeShow => String::from("\x1b[?25h"),
            VirtualTerminalSequence::TextCursorEnableModeHide => String::from("\x1b[?25l"),
            VirtualTerminalSequence::CursorPosition { cy, cx } => format!("\x1b[{};{}H", cy, cx),
            VirtualTerminalSequence::EraseInLine(n) => match n {
                Some(n) => format!("\x1b[{}K", n),
                None => String::from("\x1b[K"),
            },
        }
    }
}

struct BackBuffer {
    back_buffer: String,
}

impl BackBuffer {
    fn with_capacity(capacity: usize) -> Self {
        Self {
            back_buffer: String::with_capacity(capacity),
        }
    }

    fn push_str(&mut self, str: &str) {
        self.back_buffer.push_str(str);
    }

    fn push_seq(&mut self, seq: &VirtualTerminalSequence) {
        self.back_buffer.push_str(seq.to_command().as_str());
    }

    fn clear(&mut self) {
        self.back_buffer.clear();
    }

    fn as_str(&self) -> &str {
        self.back_buffer.as_str()
    }
}

pub struct Writer {
    back_buffer: BackBuffer,
}

impl Writer {
    pub fn with_size(rows: usize, columns: usize) -> Self {
        Self {
            back_buffer: BackBuffer::with_capacity(rows * columns),
        }
    }

    pub fn write(&mut self, str: &str) {
        self.back_buffer.push_str(str);
    }

    pub fn write_seq(&mut self, seq: &VirtualTerminalSequence) {
        self.back_buffer.push_seq(seq);
    }

    pub fn present(&mut self, term: &impl Terminal) {
        term.write(self.back_buffer.as_str())
            .expect("unable to write back buffer to screen");
        self.back_buffer.clear();
    }

    pub fn erase_till_eol(&mut self) {
        self.write_seq(&VirtualTerminalSequence::EraseInLine(Option::None));
    }

    pub fn set_cursor_pos(&mut self, cx: usize, cy: usize) {
        self.write_seq(&VirtualTerminalSequence::CursorPosition { cy, cx });
    }

    pub fn reset_cursor(&mut self) {
        self.write_seq(&VirtualTerminalSequence::ResetCursor);
    }

    pub fn hide_cursor(&mut self) {
        self.write_seq(&VirtualTerminalSequence::TextCursorEnableModeHide);
    }

    pub fn show_cursor(&mut self) {
        self.write_seq(&VirtualTerminalSequence::TextCursorEnableModeShow);
    }
}
