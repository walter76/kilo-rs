//! This module includes all the unsafe calls to to the Windows API.

extern crate winapi;

use winapi::shared::minwindef::{DWORD, FALSE};
use winapi::shared::ntdef::HANDLE;
use winapi::um::consoleapi::{
    GetConsoleMode, GetNumberOfConsoleInputEvents, ReadConsoleA, SetConsoleMode, WriteConsoleA,
};
use winapi::um::handleapi::INVALID_HANDLE_VALUE;
use winapi::um::processenv::GetStdHandle;
use winapi::um::winbase::{STD_INPUT_HANDLE, STD_OUTPUT_HANDLE, FormatMessageA};
use winapi::um::wincon::{
    self, GetConsoleScreenBufferInfo, CONSOLE_READCONSOLE_CONTROL, CONSOLE_SCREEN_BUFFER_INFO,
    COORD, SMALL_RECT,
};
use winapi::um::errhandlingapi::GetLastError;

use std::io;
use std::os::raw::c_ulong;

// TODO add error handling
fn get_last_error() {
    let err_code: DWORD = unsafe { GetLastError() };
    // FormatMessage
    // https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-formatmessage
    // custom error for windows error messages with error code and message
}

/// Used to encapsulate screen coordinates. The upper left corner is `(0,0)`.
pub struct Point {
    /// Column (or x) position on the screen.
    pub x: i16,
    /// Row (or y) position on the screen.
    pub y: i16,
}

impl Point {
    /// Takes a native `COORD` structure and converts it to `Point`.
    fn from_coord(coord: &COORD) -> Self {
        Self {
            x: coord.X,
            y: coord.Y,
        }
    }
}

/// Used to encapsulate a rectangular area on the screen.
pub struct Rect {
    pub left: i16,
    pub top: i16,
    pub right: i16,
    pub bottom: i16,
}

impl Rect {
    // Takes a native `SMALL_RECT` structure and converts it to a `Rect`.
    fn from_small_rect(small_rect: &SMALL_RECT) -> Self {
        Self {
            left: small_rect.Left,
            top: small_rect.Top,
            right: small_rect.Right,
            bottom: small_rect.Bottom,
        }
    }
}

/// Contains information about a console screen buffer.
pub struct ConsoleScreenBufferInfo {
    /// Contains the size of the console screen buffer, in character columns and
    /// rows.
    pub size: Point,
    /// Contains the column and row coordinates of the cursor in the console
    /// screen buffer.
    pub cursor_position: Point,
    /// The attributes of the characters written to a screen buffer by the
    /// WriteFile and WriteConsole functions, or echoed to a screen buffer by
    /// the ReadFile and ReadConsole functions. For more information, see the
    /// windows API documentation of `CONSOLE_SCREEN_BUFFER_INFO`.
    pub attributes: u16,
    /// Contains the console screen buffer coordinates of the upper-left and
    /// lower-right corners of the display window.
    pub display_window: Rect,
    /// Contains the maximimum size of the console window, in character columns
    /// and rows, given the current screen buffer size and font and the screen
    /// size.
    pub maximum_window_size: Point,
}

/// Get information about the console screen buffer from the operating system.
pub fn get_console_screen_buffer_info(handle: &ConsoleHandle) -> Option<ConsoleScreenBufferInfo> {
    if handle.handle_type != ConsoleHandleType::Stdout {
        panic!(
            "It is not possible to get screen buffer info from a handle type other than stdout."
        );
    }

    let mut console_screen_buffer_info = CONSOLE_SCREEN_BUFFER_INFO {
        dwSize: COORD { X: 0, Y: 0 },
        dwCursorPosition: COORD { X: 0, Y: 0 },
        wAttributes: 0x0,
        srWindow: SMALL_RECT {
            Left: 0,
            Top: 0,
            Right: 0,
            Bottom: 0,
        },
        dwMaximumWindowSize: COORD { X: 0, Y: 0 },
    };

    if unsafe { GetConsoleScreenBufferInfo(handle.raw_handle, &mut console_screen_buffer_info) }
        == FALSE
    {
        return None;
    }

    Some(ConsoleScreenBufferInfo {
        size: Point::from_coord(&console_screen_buffer_info.dwSize),
        cursor_position: Point::from_coord(&console_screen_buffer_info.dwCursorPosition),
        attributes: console_screen_buffer_info.wAttributes,
        display_window: Rect::from_small_rect(&console_screen_buffer_info.srWindow),
        maximum_window_size: Point::from_coord(&console_screen_buffer_info.dwMaximumWindowSize),
    })
}

/// Defines which type of console handle (e.g. stdout, stdin) it is.
#[derive(Debug, PartialEq)]
pub enum ConsoleHandleType {
    Stdin,
    Stdout,
}

/// Wraper for a raw handle to the console (e.g. stdout, stdin).
#[derive(Debug)]
pub struct ConsoleHandle {
    handle_type: ConsoleHandleType,
    raw_handle: HANDLE,
}

impl ConsoleHandle {
    fn new(handle_type: ConsoleHandleType, raw_handle: HANDLE) -> Self {
        Self {
            handle_type,
            raw_handle,
        }
    }

    pub fn as_raw(&self) -> &HANDLE {
        &self.raw_handle
    }
}

/// Get the handle for stdin from the operating system.
pub fn get_stdin_handle() -> Result<ConsoleHandle, io::Error> {
    match get_std_handle(&STD_INPUT_HANDLE) {
        Ok(console_handle) => Ok(ConsoleHandle::new(ConsoleHandleType::Stdin, console_handle)),
        Err(e) => Err(e),
    }
}

/// Get the handle for stdout from the operating system.
pub fn get_stdout_handle() -> Result<ConsoleHandle, io::Error> {
    match get_std_handle(&STD_OUTPUT_HANDLE) {
        Ok(console_handle) => Ok(ConsoleHandle::new(
            ConsoleHandleType::Stdout,
            console_handle,
        )),
        Err(e) => Err(e),
    }
}

/// Get a handle from the operating system.
fn get_std_handle(handle_type: &DWORD) -> Result<HANDLE, io::Error> {
    let h_std = unsafe { GetStdHandle(*handle_type) };

    if h_std == INVALID_HANDLE_VALUE {
        return Err(std::io::Error::new(
            io::ErrorKind::Other,
            "cannot get handle from operating system",
        ));
    }

    Ok(h_std)
}

bitflags! {
    /// Models the flags used for reading and setting the current console mode.
    /// See further details in the Microsoft documentation about
    /// [Console Modes](https://docs.microsoft.com/en-us/windows/console/console-modes).
    pub struct ConsoleModeFlags: c_ulong {
        const ENABLE_ECHO_INPUT = wincon::ENABLE_ECHO_INPUT;                                   // 0x0004
        const ENABLE_INSERT_MODE = wincon::ENABLE_INSERT_MODE;                                 // 0x0020
        const ENABLE_LINE_INPUT = wincon::ENABLE_LINE_INPUT;                                   // 0x0002
        const ENABLE_MOUSE_INPUT = wincon::ENABLE_MOUSE_INPUT;                                 // 0x0010
        const ENABLE_PROCESSED_INPUT = wincon::ENABLE_PROCESSED_INPUT;                         // 0x0001
        const ENABLE_QUICK_EDIT_MODE = wincon::ENABLE_QUICK_EDIT_MODE;                         // 0x0040
        const ENABLE_WINDOW_INPUT = wincon::ENABLE_WINDOW_INPUT;                               // 0x0008
        const ENABLE_VIRTUAL_TERMINAL_INPUT = wincon::ENABLE_VIRTUAL_TERMINAL_INPUT;           // 0x0200
        const ENABLE_PROCESSED_OUTPUT = wincon::ENABLE_PROCESSED_OUTPUT;                       // 0x0001
        const ENABLE_WRAP_AT_EOL_OUTPUT = wincon::ENABLE_WRAP_AT_EOL_OUTPUT;                   // 0x0002
        const ENABLE_VIRTUAL_TERMINAL_PROCESSING = wincon::ENABLE_VIRTUAL_TERMINAL_PROCESSING; // 0x0004
        const DISABLE_NEWLINE_AUTO_RETURN = wincon::DISABLE_NEWLINE_AUTO_RETURN;               // 0x0008
        const ENABLE_LVB_GRID_WORLDWIDE = wincon::ENABLE_LVB_GRID_WORLDWIDE;                   // 0x0010
        const ENABLE_EXTENDED_FLAGS = wincon::ENABLE_EXTENDED_FLAGS;                           // 0x0080
        const ENABLE_AUTO_POSITION = wincon::ENABLE_AUTO_POSITION;                             // 0x0100
    }
}

/// Reads the current mode of the console.
pub fn get_console_mode_flags(handle: &ConsoleHandle) -> Option<ConsoleModeFlags> {
    let mut console_mode: DWORD = 0x0;

    println!("{:?}", handle);
    if unsafe { GetConsoleMode(*handle.as_raw(), &mut console_mode) } == FALSE {
        return None;
    }

    println!("{:?}", console_mode);
    Some(ConsoleModeFlags::from_bits(console_mode).expect("cannot create console mode flags"))
}

/// Set the mode of the console.
pub fn set_console_mode_flags(handle: &ConsoleHandle, console_mode_flags: &ConsoleModeFlags) {
    if unsafe { SetConsoleMode(*handle.as_raw(), console_mode_flags.bits()) } == FALSE {
        panic!(
            "cannot set console mode 0x{:02X?} for handle {:?}",
            console_mode_flags, handle
        );
    }
}

/// The mode of the console.
pub struct ConsoleMode {
    console_mode_flags: ConsoleModeFlags,
}

impl ConsoleMode {
    /// Reads the current mode of the console and stores it for later use.
    pub fn from_handle(handle: &ConsoleHandle) -> Self {
        let console_mode_flags = match get_console_mode_flags(handle) {
            Some(console_mode) => console_mode,
            None => ConsoleModeFlags::empty(),
        };

        Self { console_mode_flags }
    }

    /// Takes the previously stored console mode flags and restores them in the
    /// system console.
    pub fn restore(&mut self, handle: &ConsoleHandle) {
        set_console_mode_flags(handle, &self.console_mode_flags);
    }
}

/// Read a maximum number of characters from the console.
pub fn read_console(handle: &ConsoleHandle, number_of_chars_to_read: &usize) -> Option<Vec<u8>> {
    if handle.handle_type != ConsoleHandleType::Stdin {
        panic!("It is not possible to read from a handle type other than stdin.");
    }

    let mut number_of_chars_read: DWORD = 0x0;
    let mut read_buffer: Vec<u8> = Vec::with_capacity(*number_of_chars_to_read);
    let mut input_control = CONSOLE_READCONSOLE_CONTROL {
        nLength: 0,
        nInitialChars: 0,
        dwCtrlWakeupMask: 0,
        dwControlKeyState: 0,
    };

    unsafe {
        if ReadConsoleA(
            handle.raw_handle,
            read_buffer.as_mut_ptr() as *mut winapi::ctypes::c_void,
            *number_of_chars_to_read as u32,
            &mut number_of_chars_read,
            &mut input_control,
        ) == FALSE
        {
            return None;
        }

        read_buffer.set_len(number_of_chars_read as usize);
    }

    Some(read_buffer)
}

/// Write a string to the console.
pub fn write_console(handle: &ConsoleHandle, str: &str) -> Option<u32> {
    if handle.handle_type != ConsoleHandleType::Stdout {
        panic!("It is not possible to read from a handle type other than stdout.");
    }

    let write_buffer = Vec::from(str.as_bytes());
    let number_of_chars_to_write = write_buffer.len();
    let mut number_of_chars_written: DWORD = 0x0;

    if unsafe {
        WriteConsoleA(
            handle.raw_handle,
            write_buffer.as_ptr() as *const winapi::ctypes::c_void,
            number_of_chars_to_write as u32,
            &mut number_of_chars_written,
            std::ptr::null_mut(),
        )
    } == FALSE
    {
        return None;
    }

    Some(number_of_chars_written)
}

/// Get the current number of available console input events from the operating
/// system.
pub fn get_number_of_console_input_events(handle: &ConsoleHandle) -> Option<c_ulong> {
    let mut number_of_events: DWORD = 0x0;

    if unsafe { GetNumberOfConsoleInputEvents(handle.raw_handle, &mut number_of_events) } == FALSE {
        return None;
    }

    Some(number_of_events)
}
