use crate::terminal::{Terminal, VirtualTerminalSequence, WindowSize};
use crate::win_con::win_os::{self, ConsoleHandle, ConsoleMode, ConsoleModeFlags};

pub struct WinTerm {
    stdin: ConsoleHandle,
    stdin_original_console_mode: ConsoleMode,
    stdout: ConsoleHandle,
    stdout_original_console_mode: ConsoleMode,
}

impl WinTerm {
    // TODO add some console output if console mode cannot be saved
    // TODO this needs proper error handling
    pub fn new() -> Self {
        println!("stdin");
        let stdin = win_os::get_stdin_handle().expect("Cannot get handle for stdin.");
        let stdin_original_console_mode = ConsoleMode::from_handle(&stdin);

        let stdout = win_os::get_stdout_handle().expect("Cannot get handle for stdout");
        let stdout_original_console_mode = ConsoleMode::from_handle(&stdout);

        let term = Self {
            stdin,
            stdin_original_console_mode,
            stdout,
            stdout_original_console_mode,
        };

        term.enable_virtual_terminal_sequences();
        term.enable_raw_mode();

        term
    }

    fn enable_virtual_terminal_sequences(&self) {
        let stdin_current_console_mode_flags =
            win_os::get_console_mode_flags(&self.stdin).unwrap_or_else(ConsoleModeFlags::empty);
        let stdin_console_mode_flags =
            stdin_current_console_mode_flags | ConsoleModeFlags::ENABLE_VIRTUAL_TERMINAL_INPUT;
        win_os::set_console_mode_flags(&self.stdin, &stdin_console_mode_flags);

        let stdout_current_console_mode_flags =
            win_os::get_console_mode_flags(&self.stdout).unwrap_or_else(ConsoleModeFlags::empty);
        let stdout_console_mode_flags = stdout_current_console_mode_flags
            | ConsoleModeFlags::ENABLE_VIRTUAL_TERMINAL_PROCESSING;
        win_os::set_console_mode_flags(&self.stdout, &stdout_console_mode_flags);
    }

    fn enable_raw_mode(&self) {
        let stdin_current_console_mode_flags =
            win_os::get_console_mode_flags(&self.stdin).unwrap_or_else(ConsoleModeFlags::empty);
        let stdin_console_mode_flags = stdin_current_console_mode_flags
            & !(ConsoleModeFlags::ENABLE_ECHO_INPUT
                | ConsoleModeFlags::ENABLE_LINE_INPUT
                | ConsoleModeFlags::ENABLE_PROCESSED_INPUT
                | ConsoleModeFlags::ENABLE_EXTENDED_FLAGS);
        win_os::set_console_mode_flags(&self.stdin, &stdin_console_mode_flags);

        let stdout_current_console_mode_flags =
            win_os::get_console_mode_flags(&self.stdout).unwrap_or_else(ConsoleModeFlags::empty);
        let stdout_console_mode_flags =
            // TODO for whatever reason I have to let ENABLE_PROCESSED_OUTPUT be enabled
            // stdout_current_console_mode & !(ENABLE_PROCESSED_OUTPUT | ENABLE_WRAP_AT_EOL_OUTPUT);
            // Might have todo with the codepage. Possible I have to set it:
            //
            // from https://docs.microsoft.com/en-us/windows/console/classic-vs-vt
            //
            // UTF-8 support in the console can be utilized via the A variant of Console APIs against
            // console handles after setting the codepage to 65001 or CP_UTF8 with the SetConsoleOutputCP
            // and SetConsoleCP methods, as appropriate. Setting the code pages in advance is only
            // necessary if the machine has not chosen "Use Unicode UTF-8 for worldwide language support"
            // in the settings for Non-Unicode applications in the Region section of the Control Panel.
            //
            stdout_current_console_mode_flags & !(ConsoleModeFlags::ENABLE_WRAP_AT_EOL_OUTPUT);
        win_os::set_console_mode_flags(&self.stdout, &stdout_console_mode_flags);
    }

    pub fn read_c_nb(&self) -> Option<char> {
        if let Some(number_of_console_input_events) =
            win_os::get_number_of_console_input_events(&self.stdin)
        {
            if number_of_console_input_events > 0 {
                if let Some(read_buffer) = win_os::read_console(&self.stdin, &1) {
                    return Some(read_buffer.into_iter().next()? as char);
                }
            }
        }

        None
    }

    pub fn write(&self, str: &str) -> Option<u32> {
        win_os::write_console(&self.stdout, str)
    }

    pub fn write_seq(&self, seq: &VirtualTerminalSequence) {
        let seq_str = seq.to_command();
        win_os::write_console(&self.stdout, seq_str.as_str()).unwrap_or_else(|| {
            panic!(
                "unable to write virtual terminal sequence {:?} to terminal",
                seq
            )
        });
    }

    // TODO add some console output if window size cannot be determined
    fn get_window_size(&self) -> Option<WindowSize> {
        match self.get_window_size_winapi() {
            Some(window_size) => Some(window_size),
            None => self.get_window_size_virtual_terminal(),
        }
    }

    fn get_window_size_winapi(&self) -> Option<WindowSize> {
        match win_os::get_console_screen_buffer_info(&self.stdout) {
            Some(info) => {
                let height = (info.display_window.bottom - info.display_window.top + 1) as usize;
                let width = (info.display_window.right - info.display_window.left + 1) as usize;
                Some(WindowSize { height, width })
            }
            None => None,
        }
    }

    fn get_window_size_virtual_terminal(&self) -> Option<WindowSize> {
        // move cursor to edge of screen
        // the control sequence is guaranteed to stop at edge of screen
        self.write_seq(&VirtualTerminalSequence::CursorForward(999));
        self.write_seq(&VirtualTerminalSequence::CursorDown(999));

        // get cursor position
        self.write_seq(&VirtualTerminalSequence::ReportCursorPosition);

        let mut read_buffer: Vec<char> = Vec::with_capacity(16);
        let mut is_eof = false;

        loop {
            let c = match self.read_c_nb() {
                Some(c) => c,
                None => {
                    is_eof = true;
                    0x0 as char
                }
            };

            if is_eof {
                break;
            }

            read_buffer.push(c);
            if c == 'R' {
                break;
            }
        }

        let last_index = read_buffer.len() - 1;

        if read_buffer[0] != '\x1b' || read_buffer[1] != '[' || read_buffer[last_index] != 'R' {
            return None;
        }

        let s: String = read_buffer[2..last_index].iter().collect();

        let wh: Vec<&str> = s.split(';').collect();
        let height = wh[0].parse::<usize>().expect("cannot parse height");
        let width = wh[1].parse::<usize>().expect("cannot parse width");

        Some(WindowSize { height, width })
    }
}

impl Drop for WinTerm {
    fn drop(&mut self) {
        self.stdin_original_console_mode.restore(&self.stdin);
        self.stdout_original_console_mode.restore(&self.stdout);
    }
}

impl Terminal for WinTerm {
    fn clear_screen(&self) {
        self.write_seq(&VirtualTerminalSequence::ClearScreen);
        self.write_seq(&VirtualTerminalSequence::ResetCursor);
    }

    fn write(&self, str: &str) -> Option<u32> {
        self.write(str)
    }

    fn window_size(&self) -> Option<WindowSize> {
        self.get_window_size()
    }

    fn read_c(&self) -> Option<char> {
        let read_buffer = win_os::read_console(&self.stdin, &1);
        Some(read_buffer?.into_iter().next()? as char)
    }
}
