#[macro_use]
extern crate bitflags;

mod editor;
mod terminal;
mod win_con;

use std::env;

use editor::Config;
use win_con::win_term::WinTerm;

pub const EDITOR_VERSION: &str = "0.0.1";
pub const EDITOR_DEFAULT_WIDTH: usize = 80;
pub const EDITOR_DEFAULT_HEIGHT: usize = 24;

// TODO refactor the code to make it better readable and more idiomatic
// TODO add logging

fn main() {
    let term = WinTerm::new();
    let config = Config::new(env::args());

    editor::run(&term, &config);
}
