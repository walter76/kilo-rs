# kilo-rs

This project is a pure learning experience. I try to reimplement kilo in rust. It
is similar to what is described in this blog
[Hecto: Build your own text editor in Rust](https://www.philippflenker.com/hecto/).

The difference is that in this blog the implementation is done on a Linux system
with using an external library [termion](https://crates.io/crates/termion) for
console handling. I am on a Windows system and will not try to use any library
for console handling, e.g. [crossterm](https://crates.io/crates/crossterm). Which
is most likely stupid, but my goal is to learn rust and interfacing with Windows.

I will not use the blog from Philipp Flenker, but mainly use the original tutorial
[Build Your Own Text Editor](https://viewsourcecode.org/snaptoken/kilo/index.html).

